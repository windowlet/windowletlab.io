# Atelier of Kaoruko

Site: https://windowlet.gitlab.io/

## What is this?

"Kaoruko's Atelier" is my personal and portfolio website. In short, here's where I will be posting my stories, long and short, such that you can share with others, exported in the PDF format for easy portability (it's in the name) and lack of Digital Restrictions Management (DRM) shenanigans.

Unless otherwise noted, all of my stories are licensed under the Creative Commons BY-SA 4.0 license. In short, you are allowed to do as you please with my works, even for comercial incentives. However, my name, David Klopić (David Klopic for Latin-only typesets), must be present in them because I reserve some rights to my stories. If you make a derivative work from mine, you must use the exact same license as I did.

If you like my stories, you're very welcome to share them around and optionally and if you can, please donate:

- Monero/XMR: 49cn4t7M8PcjAYJiZBuFhVEJ847LDgKoZfiustWeFoZUcvJEbPy6RsQiPmhitEQ6dFXxU2iTKtzqrQ6nxaprfAJvPz76mS8
- Liberapay: @shikanon

I do not have Paypal because they have banned me, for some reason.

If you're a bit of a critic, constructive criticism is welcome at supertest156@proton.me. Cheers!
